# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## 2.4.1
### Fixed
- Missing php intl extension

## 2.4.0
### Added
- Infection testing package

## 2.3.0
### Added
- Swoole extension

## 2.2.0
### Changed
- Updated to PHP 8.1

## 2.1.0
### Added
- Base php ini

## 2.0.0
### Changed
- Updated to Ubuntu 21.10

## 1.0.0
Initial Version
